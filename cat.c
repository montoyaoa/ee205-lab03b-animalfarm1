///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   27_JAN_2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat catDB[MAX_SPECIES];

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
struct Cat aliceTheCat;
strcpy(aliceTheCat.name, "Alice");
aliceTheCat.gender = FEMALE;
aliceTheCat.breed = MAIN_COON;
aliceTheCat.isFixed = TRUE;
aliceTheCat.weight = 12.34;
aliceTheCat.collar1_color = BLACK;
aliceTheCat.collar2_color = RED;
aliceTheCat.license = 12345;
catDB[i] = aliceTheCat;
}


/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   printf("Cat\tname = [%s]\n", catDB[i].name);
   printf("\tgender = [%s]\n", genderName(catDB[i].gender));
   printf("\tbreed = [%s]\n", breedName(catDB[i].breed));
   printf("\tisFixed = [%s]\n", fixedName(catDB[i].isFixed));
   printf("\tweight = [%f]\n", catDB[i].weight);
   printf("\tcollar color 1 = [%s]\n", colorName(catDB[i].collar1_color));
   printf("\tcollar color 2 = [%s]\n", colorName(catDB[i].collar2_color));
   printf("\tlicense = [%ld]\n", catDB[i].license);
}

char* breedName(enum CatBreeds breed){
   switch(breed){
      case MAIN_COON:
         return "Main Coon";
      case MANX:
         return "Manx";
      case SHORTHAIR:
         return "Shorthair";
      case PERSIAN:
         return "Persian";
      case SPHYNX:
         return "Sphynx";
   }
}

