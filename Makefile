###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Osiel Montoya <montoyao@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   27_JAN_2021
###############################################################################

TARGET = animalfarm
SOURCES = animals.c cat.c main.c

all: $(TARGET)

$(TARGET):
	cc -o $(TARGET) $(SOURCES)

clean:
	rm -f *.o $(TARGET)
